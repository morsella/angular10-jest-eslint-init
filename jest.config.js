const config = {
    reporters: ['default', 'jest-junit'],
    preset: 'jest-preset-angular',
    roots: ['src'],
    setupFilesAfterEnv: ['<rootDir>/jest.setup.ts'],
    testURL: 'http://localhost:4200'
};

module.exports = config;
